# /bin/bash -x

# tidy
tidy-xml () {
	tidy -xml -i -config tidy.config -m "$1"
}

tidy-xml corpus/BioNLP-ST_2011_genia_devel_data_rev1.xml
tidy-xml corpus/BioNLP-ST_2011_train_devel_data_rev1.xml
tidy-xml corpus/BioNLP-ST-2013_GE_train_data_rev3.xml
tidy-xml corpus/BioNLP-ST-2013_GE_devel_data_rev3.xml

